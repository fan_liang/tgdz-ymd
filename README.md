# 天干地支纪年月日

#### 介绍

 **PHP获取阴历阳历天干地支纪年月日生肖五行24节气生辰八字** 

#### 软件架构

 **php: >=5.6.0** 


#### 安装教程

 **composer require jokerfan/tgdz** 

#### 使用说明


```
use Jokerfan\Tgdz\TgdzYmd;

$data=new TgdzYmd();

//参数：年，月，日
return $data->solar(2021,8,7);

```

#### 返回结果及说明


```
{
    "lunar_year": "2021",（农历-年）
    "lunar_month": "06",（农历-月）
    "lunar_day": "29",（农历-日）
    "lunar_hour": null,（农历-时）
    "lunar_year_chinese": "二零二一",（农历-大写年）
    "lunar_month_chinese": "六月",（农历-大写月）
    "lunar_day_chinese": "廿九",（农历-大写日）
    "lunar_hour_chinese": null,（农历-大写时）
    "ganzhi_year": "辛丑",（干支-年）
    "ganzhi_month": "丙申",（干支-月）
    "ganzhi_day": "丁亥",（干支-日）
    "ganzhi_hour": null,（干支-时）
    "wuxing_year": "金土",（五行-年）
    "wuxing_month": "火金",（五行-月）
    "wuxing_day": "火水",（五行-日）
    "wuxing_hour": null,（五行-时）
    "color_year": "白",（颜色-年）
    "color_month": "红",（颜色-月）
    "color_day": "红",（颜色-日）
    "color_hour": null,（颜色-时）
    "animal": "牛",（生肖）
    "term": "立秋",（节气）
    "is_leap": false,（是否为闰年）
    "gregorian_year": "2021",（阳历-年）
    "gregorian_month": "08",（阳历-年）
    "gregorian_day": "07",（阳历-年）
    "gregorian_hour": null,（阳历-年）
    "week_no": 6,（周几）
    "week_name": "星期六",（周几大写）
    "is_today": false,（是否是今日）
    "constellation": "狮子"（星座）
}
```


